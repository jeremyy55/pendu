This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# Jeu du pendu
Ce jeu a été créé suite aux consignes données dans l'exercice présent dans [le cours  openclassroom sur react.js](https://openclassrooms.com/fr/courses/4664381-realisez-une-application-web-avec-react-js)

une démonstration du projet est accessible [ici](https://jeremyy55.gitlab.io/pendu/)
