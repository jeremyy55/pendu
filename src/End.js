import React from 'react';
import './End.css'

const End= ({newGame})=>(
    <div className='end'>
        <h1>CHAMPIIIIOOOONNNNNN! Elle était facile celle là!</h1> 
        <div>
            <p className='myButton'onClick={()=>newGame()}> Une nouvelle partie?</p>
        </div> 
    </div>
    
)

export default End