import React, {Component} from 'react';
import './Keyboard.css';

class Keyboard extends Component{
    state= {
        inputValue:'',
        displayingValue:'',
    }
    updateInputValue(evt){
        const {handleKeyboard}=this.props
        const oldInput=evt.target.value.toUpperCase()
        this.setState({
            inputValue:'',
            displayingValue:oldInput,
        })
        handleKeyboard(oldInput)

    }
    
    render(){
        
        return (
            <div className="Keyboard">
                <p className="KeyInput">
                    keyboard entry: <input  autoFocus value={this.state.inputValue} onChange={evt => this.updateInputValue(evt)}/>
                </p>
                
                <h1>la dernière entrée clavier: {this.state.displayingValue}</h1>
            </div>
            
        )
    }
}

export default Keyboard;