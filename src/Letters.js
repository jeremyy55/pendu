import React,{Component} from 'react';
import './Letters.css';
import PropTypes from 'prop-types';


const letters=[...'abcdefghijklmnopqrstuvwxyz'.toUpperCase()];

const Letters = ({handleClick,usedLetter}) => (
    <div className='letters'>
       {
         letters.map((letter) => (
         <Letter key={letter} handleClick={handleClick}  usedLetter={usedLetter}letter={letter} />
       )
       )} 
      </div>
)

class Letter extends Component{
    state={
        used:false,
    }

    isUsed(){
        const {letter,usedLetter}= this.props
        if (usedLetter.has(letter)){
            this.setState({used:true})
        }
    }
    componentWillReceiveProps(nextProps){
        this.isUsed()
    }

    computeClassName(){
        const {letter,usedLetter}=this.props
        return 'letter '.concat(usedLetter.has(letter)?'used':'')
    }
    handleAndUpdate= (letter) =>{
        this.props.handleClick(letter)
        this.setState({used:true})
    }
    static propTypes = {
        letter:PropTypes.string.isRequired,
    }
    render(){
        const {letter}=this.props
        return (
            <div onClick={()=>this.handleAndUpdate(letter)} className={this.state.used?'letter used':'letter'}>
                <span> {letter}</span>
            </div>
        )
    }
}



export default Letters;