import React,{Component} from 'react';
import './App.css';
import Letters from './Letters';
import End from './End'
import Keyboard from './Keyboard'

const toGuess=[
  'un','deux','trois','trois petits chats','ninja','bambou', 'filou','relou','hibou','genou','lorem ipsum',
  'les chausettes de l archiduchesse sont elles seches','mais oui c est clair','drole','sympa','amusant','Hippotapotame','mouiller','liquide','croc','cheval','c est genial',
  'tabou','dogme','rayon','cosmique','chakra','note','grigri']
function computeDisplay(elementToGuess,usedLetter){
  return elementToGuess.replace(/\w/g,
    (letter) => (usedLetter.has(letter) ? letter : '_')
    )
}
 class App extends Component {
  initialState(){
    return {
      elementToGuess:this.generatesElementToGuess(),
      usedLetter:new Set([' ']),
      guesses:0,
      score:0,   
    }
  }
  state =  this.initialState()

  /** arrow fx for binding*/
  newGame = () => {
    console.log('in new Game')
    console.log(this.state)
    this.setState(this.initialState())
  }
  /**
  the function is responsible of :
    incrementing the amount of guess
    adding the letter in letter used if it is required
    computing the score : +2 if correct, -1 if uncorrect, -2 if an incorrect one is reused
  nb: the arrow function is used for binding this
  */  
  handleClick = (letter) => { //arrow function for binding this
    const {usedLetter,guesses,score,elementToGuess} = this.state
    const newGuesses = guesses+1
    //letter=letter.toUpperCase() //format keyboard input // we should controll the input 
    this.setState({guesses:newGuesses,})
    if (usedLetter.has(letter)){
      const newScore=score-2
      this.setState({score:newScore})
    }else{
      const newUsedLetter=usedLetter.add(letter)
      this.setState({usedLetter:newUsedLetter})

      const LettersToGuess= new Set([...elementToGuess])
      console.log('LettersToGuess: ',LettersToGuess)
      console.log('letter:',letter)
      if(LettersToGuess.has(letter)){
        const newScore = score +2
        this.setState({score:newScore})
      }else{
        const newScore = score -1
        this.setState({score:newScore})
      }
    }
    
    return
  }


  generatesElementToGuess(){
    return toGuess[Math.floor(Math.random()* toGuess.length)].toUpperCase()
  }

  render() {
    const {elementToGuess,guesses,usedLetter,score} = this.state
    const won = elementToGuess.replace(/\w|\s/g,
      (letter) => (
        usedLetter.has(letter) ? '' : '_')
      ) === '' 
    return (
    <div className="App">
      <h1 > Ce bon vieux jeu du pendu à commencé! </h1>
      <div className='riddle'>
        <h2>Voici la devinette pour laquelle tu risques ta vie: </h2>
        <p className="toGuess">
          {computeDisplay(elementToGuess,usedLetter)}
        </p>
      </div>
      {/*can become a component*/}
      <div>
        <p className="info"> essais: {guesses}<br/>score:{score}

        </p>
      </div>
        {won ? 
        <End newGame={this.newGame}/> :
        (
          <div>
            <Letters  handleClick={this.handleClick} usedLetter={usedLetter} />
            <Keyboard handleKeyboard={this.handleClick}/>
          </div>
        )}   
    </div>
  );
}
  
}

export default App;
